import React from 'react';
import './App.css';

class App extends React.Component {
  state: any = {
    timer: 0,
    result: [],
    userTurnIndex: -1,
    totalTime: 0,
    totalUsed: 0,
  }

  props: any;

  interval: any = null;
  intervalDailyMeeting: any = null;

  constructor(props: any) {
    super(props);
    this.start = this.start.bind(this);
    this.lap = this.lap.bind(this);
    this.reset = this.reset.bind(this);
  }

  start() {
    if (!this.interval) {
      const { userTurnIndex } = this.state;
      this.setState({
        userTurnIndex: userTurnIndex + 1,
      })

      this.interval = setInterval(() => {
        let { timer, totalUsed } = this.state;
        timer += 1;
        totalUsed += 1;
        this.setState({
          timer,
          totalUsed,
        });
      }, 1000);
    }

    if (!this.intervalDailyMeeting) {
      this.intervalDailyMeeting = setInterval(() => {
        let { totalTime } = this.state;
        totalTime += 1;
        this.setState({
          totalTime,
        });
      }, 1000);
    }
  }

  lap() {
    clearInterval(this.interval);
    this.interval = null;
    let { result, userTurnIndex, timer } = this.state;
    const { timeBox } = this.props;

    result[userTurnIndex] = timeBox - timer;
    this.setState({
      timer: 0,
    });
  }

  reset() {
    clearInterval(this.interval);
    clearInterval(this.intervalDailyMeeting);
    this.interval = null;
    this.intervalDailyMeeting = null;
    this.setState({
      timer: 0,
      result: [],
      userTurnIndex: -1,
      totalTime: 0,
      totalUsed: 0,
    });
  }

  render() {
    const { timer, result, totalUsed, totalTime } = this.state;
    const { timeBox } = this.props;

    return (
      <div className="App">
        <p>Total time (include delay time): {totalTime}</p>
        <p>Total time used: {totalUsed}</p>
        <p>Countdown: {timeBox - timer}</p>
        <button type="button" id="start" onClick={this.start} className="start">Start</button>&nbsp;&nbsp;
        {timer !== 0 && (
          <button type="button" id="lap" onClick={this.lap} className="start">Lap</button>
        )}
        <ul>
          {Array.isArray(result) && result.length > 0 && result.map((item, index) => {
            return <li style={{ color: item >= 0 ? 'green' : 'red' }} key={index}>User {index + 1}: {item} second{item === 1 ? '' : 's'} remaining</li>
          })}
        </ul>

        {this.intervalDailyMeeting !== null && (
          <button type="button" id="reset" onClick={this.reset} className="reset">Reset</button>
        )}
      </div>
    );
  }
}

export default App;
