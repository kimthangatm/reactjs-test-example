import React from 'react';
import App from './App';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

Enzyme.configure({ adapter: new Adapter() });

jest.useFakeTimers();

it('renders without crashing', () => {
  const wrapper = shallow(<App timeBox={5} />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

it('countdown decrement and Lap button is display, result is empty array', () => {
  const wrapper = shallow(<App timeBox={5} />);
  
  // Click "Start" button to start countdown
  wrapper.find('#start').simulate('click');
  jest.runTimersToTime(1500);
  // Click "Lap" button
  // wrapper.find('#lap').simulate('click');
  // console.log(wrapper.state());

  expect(wrapper.state()).toMatchSnapshot();
  expect(toJson(wrapper)).toMatchSnapshot();
});


it('countdown decrement and Lap button is hidden, result is have an item', () => {
  const wrapper = shallow(<App timeBox={5} />);
  
  // Click "Start" button to start countdown
  wrapper.find('#start').simulate('click');
  jest.runTimersToTime(1500);
  // Click "Lap" button
  wrapper.find('#lap').simulate('click');

  expect(wrapper.state()).toMatchSnapshot();
  expect(toJson(wrapper)).toMatchSnapshot();
});


it('reset work correctly', () => {  
  const wrapper = shallow(<App timeBox={5} />);
  
  // Click "Start" button to start countdown
  wrapper.find('#start').simulate('click');
  jest.runTimersToTime(1500);
  // Click "Lap" button
  wrapper.find('#lap').simulate('click');
  wrapper.find('#reset').simulate('click');

  expect(wrapper.state()).toMatchSnapshot();

  // No: Lap button
  // No: Reset button
  expect(toJson(wrapper)).toMatchSnapshot();
});